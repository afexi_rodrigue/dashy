import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { InformationService } from '../information.service';
import { Information } from '../information';
import { Commune } from '../commune';
import { Typeinformation } from '../typeinformation';
import { FormsModule } from '@angular/forms';


@Component({
  selector: 'app-ajouter-information',
  templateUrl: './ajouter-information.component.html',
  styleUrls: ['./ajouter-information.component.scss']
})
export class AjouterInformationComponent implements OnInit {
  id=this.actRoute.snapshot.params['id'];
  informations: any;
  infos: Information = new Information();
  commune!: Commune[];
  typeinformation!: Typeinformation[]

  
  
  
  constructor(private informationService: InformationService,private router: Router,
    private actRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllInfos();
    this.saveInfo();
    this.getCommunes();
    this.getTypeInfos();
  }

  getAllInfos() {
    this.informationService.list()
      .subscribe(
        (informations: any) => {
          this.informations = informations;
        },
        (error: any) => {
          console.log(error);
        });
  }

  private getCommunes(){
    this.informationService.getCommunes().subscribe(data => {
      this.commune = data;
    });
  }
  private getTypeInfos(){
    this.informationService.getTypeInfos().subscribe(data => {
      this.typeinformation = data;
    });
  }

  saveInfo(){
    this.informationService.create(this.infos).subscribe( data =>{
      console.log(data);
      this.goToInfoList();
    },
    error => console.log(error));
  }

  goToInfoList(){
    this.router.navigate(['/informations/liste-informations']);
  }

  

  onSubmit(){
    console.log(this.infos);
    this.saveInfo();
  }
  

}
