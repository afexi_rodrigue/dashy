import { Component, OnInit } from '@angular/core';
import { InformationService } from '../information.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-informations',
  templateUrl: './liste-informations.component.html',
  styleUrls: ['./liste-informations.component.scss']
})
export class ListeInformationsComponent implements OnInit {
  infos: any;
  constructor(private informationService: InformationService,private router: Router) { }

  ngOnInit(): void {
    this.getAllInfos();
  }

  getAllInfos() {
    this.informationService.list()
      .subscribe(
        (infos: any) => {
          this.infos = infos;
        },
        (error: any) => {
          console.log(error);
        });
  }

}
