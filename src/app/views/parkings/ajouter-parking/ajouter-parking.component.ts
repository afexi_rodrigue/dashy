import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ParkingService } from '../parking.service';
import { Parking } from '../parking';
import { Commune } from '../commune';

@Component({
  selector: 'app-ajouter-parking',
  templateUrl: './ajouter-parking.component.html',
  styleUrls: ['./ajouter-parking.component.scss']
})
export class AjouterParkingComponent implements OnInit {
  id=this.actRoute.snapshot.params['id'];
  parkings1: any; //parkings1
  parkings: Parking = new Parking() //parkings
  communes!: any[]

  constructor(private parkingService: ParkingService,private router: Router,
    private actRoute: ActivatedRoute) { }

    ngOnInit(): void {
      this.getAllParkings();
      this.saveParking();
      this.getAllCommunes();
    }

    getAllParkings() {
      this.parkingService.list()
        .subscribe(
          (parkings1: any) => {
            this.parkings1 = parkings1;
          },
          (error: any) => {
            console.log(error);
          });
    }

    getAllCommunes(){
      this.parkingService.getCommunes()
        .subscribe(
          data => {
            this.communes = data;
          },
          (error: any) => {
            console.log(error);
          });
      
    }

    saveParking(){
      this.parkingService.create(this.parkings).subscribe( data =>{
        console.log(data);
        this.goToParkingList();
      },
      error => console.log(error));
    }

    goToParkingList(){
      this.router.navigate(['/parkings/liste-parkings']);
    }
  
    onSubmit(){
      console.log(this.parkings);
      this.saveParking();
    }

}
