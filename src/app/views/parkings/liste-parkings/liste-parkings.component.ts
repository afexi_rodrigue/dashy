import { Component, OnInit } from '@angular/core';
import { ParkingService } from '../parking.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-parkings',
  templateUrl: './liste-parkings.component.html',
  styleUrls: ['./liste-parkings.component.scss']
})
export class ListeParkingsComponent implements OnInit {

  parks: any;
  constructor(private parkingService: ParkingService,private router: Router) { }

  ngOnInit(): void {
    this.getAllParkings();
  }

  getAllParkings() {
    this.parkingService.list()
      .subscribe(
        (parks: any) => {
          this.parks = parks;
        },
        (error: any) => {
          console.log(error);
        });
  }

  deleteParking(id: number){
    this.parkingService.delete(id).subscribe( data => {
      console.log(data.id);
      this.getAllParkings();
    })
  }

  updateParking(id: number){
    this.router.navigate(['modifier-parking', id]);
  }

}
