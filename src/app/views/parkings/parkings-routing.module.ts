import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AjouterParkingComponent } from './ajouter-parking/ajouter-parking.component';
import { ListeParkingsComponent } from './liste-parkings/liste-parkings.component';


const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Parkings'
    },
    children: [
      {
        path: '',
        redirectTo: 'ajouter-parking'
      },
      {
        path: 'ajouter-parking',
        component:AjouterParkingComponent,
        data: {
          title: 'Ajouter un parking'
        }
      },
      {
        path: 'liste-parkings',
        component: ListeParkingsComponent,
        data: {
          title: 'Liste des parkings'
        }
      },
      
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ParkingsRoutingModule { }



