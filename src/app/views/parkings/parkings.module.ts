import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AjouterParkingComponent } from './ajouter-parking/ajouter-parking.component';
import { ListeParkingsComponent } from './liste-parkings/liste-parkings.component';
import { FormsModule, ReactiveFormsModule} from '@angular/forms';



import { ParkingsRoutingModule} from './parkings-routing.module';


@NgModule({
  declarations: [
    AjouterParkingComponent,
    ListeParkingsComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ParkingsRoutingModule,
    ReactiveFormsModule
  ]
})
export class ParkingsModule { }
