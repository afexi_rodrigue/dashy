import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { ProjetService } from '../projet.service';
import { Projet } from '../projet';

@Component({
  selector: 'app-ajout-projet',
  templateUrl: './ajout-projet.component.html',
  styleUrls: ['./ajout-projet.component.scss']
})
export class AjoutProjetComponent implements OnInit {
  id=this.actRoute.snapshot.params['id'];
  projects: any; //parkings1
  projets: Projet = new Projet(); //parkings
 

  constructor(private projetService: ProjetService,private router: Router,
    private actRoute: ActivatedRoute) { }

  ngOnInit(): void {
    this.getAllProjets();
    this.saveProjet();
  }

  getAllProjets() {
    this.projetService.list()
      .subscribe(
        (projects: any) => {
          this.projects = projects;
        },
        (error: any) => {
          console.log(error);
        });
  }

  saveProjet(){
    this.projetService.create(this.projets).subscribe( data =>{
      console.log(data);
      this.goToProjetList();
    },
    error => console.log(error));
  }

  goToProjetList(){
    this.router.navigate(['/projets']);
  }

  onSubmit(){
    console.log(this.projets);
    this.saveProjet();
  }

}

