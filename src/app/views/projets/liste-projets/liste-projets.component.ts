import { Component, OnInit } from '@angular/core';
import { ProjetService } from '../projet.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-liste-projets',
  templateUrl: './liste-projets.component.html',
  styleUrls: ['./liste-projets.component.scss']
})
export class ListeProjetsComponent implements OnInit {
  projs: any;
  constructor(private projetService: ProjetService,private router: Router) { }

  ngOnInit(): void {
    this.getAllProjets();
  }

  getAllProjets() {
    this.projetService.list()
      .subscribe(
        (projs: any) => {
          this.projs = projs;
        },
        (error: any) => {
          console.log(error);
        });
  }

  deleteProjet(id: number){
    this.projetService.delete(id).subscribe( data => {
      console.log(data.id);
      this.getAllProjets();
    })
  }

  updateProjet(id: number){
    this.router.navigate(['modifier-projet', id]);
  }





}
