import { Component, OnInit } from '@angular/core';
import { ProjetService } from '../projet.service';
import { Projet } from '../projet';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-modifier-projet',
  templateUrl: './modifier-projet.component.html',
  styleUrls: ['./modifier-projet.component.scss']
})
export class ModifierProjetComponent implements OnInit {

  id: any;
  projet: Projet = new Projet();

  constructor(private projetService: ProjetService,
    private route: ActivatedRoute,
    private router: Router) { }

  ngOnInit(): void {
    this.id = this.route.snapshot.params['id'];

    this.projetService.getProjetById(this.id).subscribe(data => {
      this.projet= data;
    }, error => console.log(error));
  }

  onSubmit(){
    this.projetService.update(this.id, this.projet).subscribe( data =>{
      this.goToProjetList();
    }
    , error => console.log(error));
  }
 
  goToProjetList(){
    this.router.navigate(['/projets']);
  }

}
